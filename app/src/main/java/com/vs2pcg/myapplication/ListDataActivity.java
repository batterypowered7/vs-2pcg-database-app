package com.vs2pcg.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;


public class ListDataActivity extends AppCompatActivity
{

    private static final String TAG = "ListDataActivity";

    DatabaseHelper mDatabaseHelper;
    DatabaseAccess mDatabaseAccess;

    private ListView mListView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);
        mListView = (ListView) findViewById(R.id.listView);
        mDatabaseHelper = new DatabaseHelper(this);
        mDatabaseAccess = DatabaseAccess.getInstance(this);
        populateListView();
    }

    private void populateListView()
    {
        Log.d(TAG, "populateListView: Displaying data in the ListView.");

        //get the data and append to a list
        mDatabaseAccess.open();
        Cursor data = mDatabaseAccess.getData();
        ArrayList<String> listData = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        while(data.moveToNext())
        {
            if(data.getString(3).equals("Main Character"))
            {
                stringBuilder.append("Level: " + data.getString(4) + "\n");
            }
            if(data.getString(3).equals("Supporting Character"))
            {
                stringBuilder.append("Cost: " + data.getString(5) + "\n");
            }
            stringBuilder.append("Team: " + data.getString(6) + "\n");
            stringBuilder.append("Card Name: " + data.getString(2) + "\n");
            stringBuilder.append("Card Type: " + data.getString(3) + "\n");
            if(data.getString(3).equals("Main Character") || data.getString(3).equals("Supporting Character"))
            {
                stringBuilder.append("Attack: " + data.getString(7) + " | " + "Defense: " + data.getString(8) + " | " + "Health: " + data.getString(9) + "\n");
                stringBuilder.append("Flight: " + data.getString(10).toUpperCase() + " | " + "Range: " + data.getString(11).toUpperCase() + "\n");
            }
            stringBuilder.append(data.getString(0));
            listData.add(stringBuilder.toString());
            stringBuilder.setLength(0);
        }

        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i,long l)
            {
                int itemLength = adapterView.getItemAtPosition(i).toString().length();
                String cardID = adapterView.getItemAtPosition(i).toString().substring(itemLength-7,itemLength);
                System.out.println(cardID);

                Log.d(TAG, "onItemClick: The ID is: " + cardID);
                Intent editScreenIntent = new Intent(ListDataActivity.this, EditDataActivity.class);
                editScreenIntent.putExtra("id",cardID);
                startActivity(editScreenIntent);
            }
        });
        mDatabaseAccess.close();
    }
}
