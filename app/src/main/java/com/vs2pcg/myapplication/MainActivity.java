package com.vs2pcg.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    DatabaseHelper mDatabaseHelper;
    private Button btnViewData;
    private EditText editText;

    private TextView universe;
    boolean[] selectedUniverse;
    ArrayList<Integer> universeList = new ArrayList<>();
    String[] universeArray;

    private TextView type;
    boolean[] selectedType;
    ArrayList<Integer> typeList = new ArrayList<>();
    String[] typeArray;

    private TextView resources;
    boolean[] selectedResource;
    ArrayList<Integer> resourceList = new ArrayList<>();
    String[] resourceArray;

    private TextView cost;
    boolean[] selectedCost;
    ArrayList<Integer> costList = new ArrayList<>();
    String[] costArray;

    private TextView set;
    boolean[] selectedSet;
    ArrayList<Integer> setList = new ArrayList<>();
    String[] setArray;

    private TextView faction;
    boolean[] selectedFaction;
    ArrayList<Integer> factionList = new ArrayList<>();
    String[] factionArray;

    private TextView attack;
    boolean[] selectedAttack;
    ArrayList<Integer> attackList = new ArrayList<>();
    String[] attackArray;

    private TextView defense;
    boolean[] selectedDefense;
    ArrayList<Integer> defenseList = new ArrayList<>();
    String[] defenseArray;

    private TextView health;
    boolean[] selectedHealth;
    ArrayList<Integer> healthList = new ArrayList<>();
    String[] healthArray;

    private TextView level;
    boolean[] selectedLevel;
    ArrayList<Integer> levelList = new ArrayList<>();
    String[] levelArray;

    private TextView keyword;
    boolean[] selectedKeyword;
    ArrayList<Integer> keywordList = new ArrayList<>();
    String[] keywordArray;

    private TextView traits;
    boolean[] selectedTraits;
    ArrayList<Integer> traitsList = new ArrayList<>();
    String[] traitsArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources res = getResources();
        universeArray = res.getStringArray(R.array.universe);
        typeArray = res.getStringArray(R.array.types);
        resourceArray = res.getStringArray(R.array.resources);
        costArray = res.getStringArray(R.array.cost);
        setArray = res.getStringArray(R.array.set);
        factionArray = res.getStringArray(R.array.factions);
        attackArray = res.getStringArray(R.array.combat);
        defenseArray = res.getStringArray(R.array.combat);
        healthArray = res.getStringArray(R.array.health);
        levelArray = res.getStringArray(R.array.level);
        keywordArray = res.getStringArray(R.array.keyword);
        traitsArray = res.getStringArray(R.array.traits);

        editText = (EditText) findViewById(R.id.editText);
        btnViewData = (Button) findViewById(R.id.btnView);
        mDatabaseHelper = new DatabaseHelper(this);

        universe = (TextView) findViewById(R.id.universe);
        selectedUniverse = new boolean[universeArray.length];
        type = (TextView) findViewById(R.id.type);
        selectedType = new boolean[typeArray.length];
        resources = (TextView) findViewById(R.id.resources);
        selectedResource = new boolean[resourceArray.length];
        cost = (TextView) findViewById(R.id.cost);
        selectedCost = new boolean[costArray.length];
        set = (TextView) findViewById(R.id.set);
        selectedSet = new boolean[setArray.length];
        faction = (TextView) findViewById(R.id.faction);
        selectedFaction = new boolean[factionArray.length];
        attack = (TextView) findViewById(R.id.attack);
        selectedAttack = new boolean[attackArray.length];
        defense = (TextView) findViewById(R.id.defense);
        selectedDefense = new boolean[defenseArray.length];
        health = (TextView) findViewById(R.id.health);
        selectedHealth = new boolean[healthArray.length];
        level = (TextView) findViewById(R.id.level);
        selectedLevel = new boolean[levelArray.length];
        keyword = (TextView) findViewById(R.id.keyword);
        selectedKeyword = new boolean[keywordArray.length];
        traits = (TextView) findViewById(R.id.traits);
        selectedTraits = new boolean[traitsArray.length];

        BuildDropdown(universe, universeArray, selectedUniverse, universeList, "Universe");
        BuildDropdown(type, typeArray, selectedType, typeList, "Type");
        BuildDropdown(resources, resourceArray, selectedResource, resourceList, "Resources");
        BuildDropdown(cost, costArray, selectedCost, costList, "Cost");
        BuildDropdown(set, setArray, selectedSet, setList, "Set");
        BuildDropdown(faction, factionArray, selectedFaction, factionList, "Faction");
        BuildDropdown(attack, attackArray, selectedAttack, attackList, "Attack");
        BuildDropdown(defense, defenseArray, selectedDefense, defenseList, "Defense");
        BuildDropdown(health, healthArray, selectedHealth, healthList, "Health");
        BuildDropdown(level, levelArray, selectedLevel, levelList, "Level");
        BuildDropdown(keyword, keywordArray, selectedKeyword, keywordList, "Keyword");
        BuildDropdown(traits, traitsArray, selectedTraits, traitsList, "Traits");

    btnViewData.setOnClickListener(new View.OnClickListener()
    {
        public void onClick(View v)
        {
            Intent intent = new Intent(MainActivity.this, ListDataActivity.class);
            startActivity(intent);
        }
    });

    }

    public void BuildDropdown(TextView textView, String[] xArray, boolean[] selectedX, ArrayList<Integer> xList, String title)
    {
        textView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setTitle(title);

                builder.setCancelable(false);

                builder.setMultiChoiceItems(xArray, selectedX, new DialogInterface.OnMultiChoiceClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i, boolean b)
                    {
                        if(b)
                        {
                            xList.add(i);
                            Collections.sort(xList);
                        }
                        else
                        {
                            int j = xList.indexOf(i);
                            xList.remove(j);
                        }
                    }
                });

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        StringBuilder stringBuilder = new StringBuilder();
                        for(int j = 0; j < xList.size(); j++)
                        {
                            stringBuilder.append(xArray[xList.get(j)]);
                            if(j != xList.size()-1)
                            {
                                stringBuilder.append(", ");
                            }
                        }

                        textView.setText(stringBuilder.toString());
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.dismiss();
                    }
                });

                builder.setNeutralButton("Clear", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        for(int j = 0; j < selectedX.length; ++j)
                        {
                            selectedX[j] = false;
                        }
                        xList.clear();
                        textView.setText("");
                    }
                });

                builder.show();
            }
        });
    }
}
