package com.vs2pcg.myapplication;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class EditDataActivity extends AppCompatActivity
{

    private static final String TAG = "EditDataActivity";

    private ListView editable_item;
    DatabaseHelper mDatabaseHelper;
    DatabaseAccess mDatabaseAccess;
    private String selectedID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_data_layout);

        editable_item = (ListView) findViewById(R.id.editable_item);
        mDatabaseHelper = new DatabaseHelper(this);
        mDatabaseAccess = DatabaseAccess.getInstance(this);

        Intent receivedIntent = getIntent();
        selectedID = receivedIntent.getStringExtra("id");

        mDatabaseAccess.open();
        Cursor data = mDatabaseAccess.getData(selectedID);
        ArrayList<String> listData = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        while(data.moveToNext())
        {
            if(data.getString(3).equals("Main Character"))
            {
                stringBuilder.append("Level: " + data.getString(4) + "\n");
            }
            if(data.getString(3).equals("Supporting Character"))
            {
                stringBuilder.append("Cost: " + data.getString(5) + "\n");
            }
            stringBuilder.append("Team: " + data.getString(6) + "\n");
            stringBuilder.append("Card Name: " + data.getString(2) + "\n");
            stringBuilder.append("Card Type: " + data.getString(3) + "\n\n");
            stringBuilder.append("----------\n" + data.getString(12) + "\n----------\n\n");
            if(data.getString(3).equals("Main Character") || data.getString(3).equals("Supporting Character"))
            {
                stringBuilder.append("Attack: " + data.getString(7) + " | " + "Defense: " + data.getString(8) + " | " + "Health: " + data.getString(9) + "\n");
                stringBuilder.append("Flight: " + data.getString(10).toUpperCase() + " | " + "Range: " + data.getString(11).toUpperCase() + "\n\n");
            }
            stringBuilder.append("Set: " + data.getString(1) + " | " + "Collector Number: " + data.getString(0));
            listData.add(stringBuilder.toString());
            ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
            editable_item.setAdapter(adapter);
        }
    }
}
