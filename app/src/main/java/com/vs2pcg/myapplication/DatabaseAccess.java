package com.vs2pcg.myapplication;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseAccess
{
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    //Private constructor to avoid object creation from outside classes.
    private DatabaseAccess(Context context)
    {
        this.openHelper = new DatabaseHelper(context);
    }

    //Return a single instance of DatabaseAccess.
    public static DatabaseAccess getInstance(Context context)
    {
        if(instance == null)
        {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    //Open the database connection.
    public void open()
    {
        this.database = openHelper.getWritableDatabase();
    }

    //Close the database connection.
    public void close()
    {
        if(database != null)
        {
            this.database.close();
        }
    }

    public Cursor getData()
    {
        String query = "SELECT * FROM CARDS";
        Cursor data = database.rawQuery(query, null);
        return data;
    }

    public Cursor getData(String cardID)
    {
        String query = "SELECT * FROM CARDS WHERE ID = '" + cardID + "'";
        Cursor data = database.rawQuery(query, null);
        return data;
    }
}
